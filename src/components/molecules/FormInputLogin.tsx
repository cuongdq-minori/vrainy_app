import auth from "@react-native-firebase/auth";
import React, { Fragment, useLayoutEffect, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Button from "../../components/atoms/Button";
import TextInput from "../../components/atoms/TextInput";
import { theme } from "../../core/theme";
import { LoginProps } from "../../navigator";

export const FormInputLogin = (props: LoginProps): JSX.Element => {
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });

  const _onLoginPressed = () => {
    auth()
      .signInWithEmailAndPassword(email.value, password.value)
      .then(() => {
        props.navigation.navigate("Home");
      })
      .catch((error) => {
        if (error.toString().includes("password"))
          return setPassword({ ...password, error: error.toString() });
        else return setEmail({ ...email, error: error.toString() });
      });
  };

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerShown: false,
      headerStatusBarHeight: 1,
    });
  }, []);

  return (
    <Fragment>
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: "" })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: "" })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />
      <View style={styles.forgotPassword}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("ForgotPass")}
        >
          <Text style={styles.label}>Quên mật khẩu?</Text>
        </TouchableOpacity>
      </View>
      <Button style={{}} mode="contained" onPress={_onLoginPressed}>
        Đăng nhập
      </Button>
      <View style={styles.row}>
        <Text style={styles.label}>Bạn chưa có tài khoản? </Text>
        <TouchableOpacity onPress={() => props.navigation.navigate("Register")}>
          <Text style={styles.link}>Đăng kí</Text>
        </TouchableOpacity>
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  forgotPassword: {
    width: "100%",
    alignItems: "flex-end",
    marginBottom: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
});
