import React, { memo } from "react";
import { Image, StyleSheet, TouchableOpacity } from "react-native";

const BackButton = ({ goBack }) => (
  <TouchableOpacity onPress={goBack} style={styles.container}>
    <Image
      style={styles.image}
      source={require("../../assets/arrow_back.png")}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    left: 10,
  },
  image: {
    width: 24,
    height: 24,
  },
});

export default memo(BackButton);
