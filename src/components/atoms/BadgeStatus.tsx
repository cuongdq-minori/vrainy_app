import React, { memo } from "react";
import { Badge } from "react-native-paper";

interface BadgeStatusProps {
  statusRain: string;
  color?: string;
}
const BadgeStatus = ({ statusRain, color }: BadgeStatusProps) => (
  <Badge visible={true} size={28} style={{ backgroundColor: color }}>
    {statusRain}
  </Badge>
);

export default memo(BadgeStatus);
