import { NavigationContainer, RouteProp } from "@react-navigation/native";
import {
  createStackNavigator,
  StackNavigationProp,
} from "@react-navigation/stack";
import React from "react";
import { Provinces } from "../interfaces/interface";
import ForgotPassWord from "../pages/forgot-password/ForgotPassWord";
import { Home } from "../pages/home/index";
import { DetailCity } from "../pages/item-detail/DetailDistrict";
import { DistrictList } from "../pages/list-district/DistrictList";
import { LoginAuthor } from "../pages/login/index";
import Register from "../pages/register/Register";

export type RootStackParamList = {
  Login: undefined;
  ForgotPass: undefined;
  Register: undefined;
  Home: undefined;
  DistrictList: { headerName: string; item?: Provinces[] };
  Detail: { headerName: string; detail: Provinces };
};

export type LoginProps = {
  route: RouteProp<RootStackParamList, "Login">;
  navigation: StackNavigationProp<RootStackParamList, "Login">;
};

export type ForgotPassProps = {
  route: RouteProp<RootStackParamList, "ForgotPass">;
  navigation: StackNavigationProp<RootStackParamList, "ForgotPass">;
};

export type RegisterProps = {
  route: RouteProp<RootStackParamList, "Register">;
  navigation: StackNavigationProp<RootStackParamList, "Register">;
};

export type HomeProps = {
  route: RouteProp<RootStackParamList, "Home">;
  navigation: StackNavigationProp<RootStackParamList, "Home">;
};

export type DistrictListProps = {
  route: RouteProp<RootStackParamList, "DistrictList">;
  navigation: StackNavigationProp<RootStackParamList, "DistrictList">;
};

export type DetailProps = {
  route: RouteProp<RootStackParamList, "Detail">;
  navigation: StackNavigationProp<RootStackParamList, "Detail">;
};

const Stack = createStackNavigator();

export function AppContainer(): JSX.Element {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={"Home"}
        screenOptions={
          {
            // gestureEnabled: true,
          }
        }
      >
        <Stack.Screen
          name="Login"
          component={LoginAuthor}
          options={{
            title: "VRainy",
          }}
        />

        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: "Trang chủ",
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
        <Stack.Screen
          name="DistrictList"
          component={DistrictList}
          options={{
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
        <Stack.Screen
          name="Detail"
          component={DetailCity}
          options={{
            headerTintColor: "white",
            headerStyle: { backgroundColor: "#2f4777" },
          }}
        />
        <Stack.Screen
          name="ForgotPass"
          component={ForgotPassWord}
          options={{
            title: "Quên mật khẩu",
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: "Đăng kí ",
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
