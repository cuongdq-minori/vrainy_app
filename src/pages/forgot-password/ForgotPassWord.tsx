import React, { memo, useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import Background from "../../components/atoms/Background";
import Button from "../../components/atoms/Button";
import Header from "../../components/atoms/Header";
import Logo from "../../components/atoms/Logo";
import TextInput from "../../components/atoms/TextInput";
import { theme } from "../../core/theme";

const ForgotPasswordScreen = ({ navigation }) => {
  const [email, setEmail] = useState({ value: "", error: "" });

  const _onSendPressed = () => {
    // const emailError = emailValidator(email.value);

    // if (emailError) {
    //   setEmail({ ...email, error: emailError });
    //   return;
    // }

    navigation.navigate("Login");
  };

  return (
    <SafeAreaView>
      <ScrollView>
        <Background>
          <Logo />

          <Header>Quên mật khẩu</Header>

          <TextInput
            label="E-mail address"
            returnKeyType="done"
            value={email.value}
            onChangeText={(text) => setEmail({ value: text, error: "" })}
            error={!!email.error}
            errorText={email.error}
            autoCapitalize="none"
            autoCompleteType="email"
            textContentType="emailAddress"
            keyboardType="email-address"
          />

          <Button
            mode="contained"
            onPress={_onSendPressed}
            style={styles.button}
          >
            Xác nhận mật khẩu
          </Button>

          <TouchableOpacity
            style={styles.back}
            onPress={() => navigation.navigate("Login")}
          >
            <Text style={styles.label}>← Trở về</Text>
          </TouchableOpacity>
        </Background>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  back: {
    width: "100%",
    marginTop: 12,
  },
  button: {
    marginTop: 12,
  },
  label: {
    color: theme.colors.secondary,
    width: "100%",
  },
});

export default memo(ForgotPasswordScreen);
