import React, { useLayoutEffect } from "react";
import { RefreshControl, SafeAreaView, ScrollView } from "react-native";
import { ListItem } from "../../components/organisms/ListItem";
import { Provinces } from "../../interfaces/interface";
import { DistrictListProps } from "../../navigator";
import styles from "../home/style/styles";

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};
export const DistrictList = (props: DistrictListProps): JSX.Element => {
  const { item: DetailCity, headerName } = props.route.params;
  const onPressToDetail = (district: Provinces) => {
    props.navigation.navigate("Detail", {
      headerName: district.name,
      detail: district,
    });
  };

  useLayoutEffect(() => {
    props.navigation.setOptions({ headerTitle: headerName });
  }, []);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <SafeAreaView style={styles.home}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <ListItem DataSource={DetailCity} onPress={onPressToDetail}></ListItem>
      </ScrollView>
    </SafeAreaView>
  );
};
