import React, { memo } from "react";
import { SafeAreaView, ScrollView } from "react-native";
import Background from "../../components/atoms/Background";
import Header from "../../components/atoms/Header";
import FormInputRegister from "../../components/molecules/FormInputRegister";
import { RegisterProps } from "../../navigator";

const RegisterScreen = (props: RegisterProps) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <Background>
          <Header>Tạo tài khoản</Header>
          <FormInputRegister {...props} />
        </Background>
      </ScrollView>
    </SafeAreaView>
  );
};

export default memo(RegisterScreen);
